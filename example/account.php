<?php
abstract class Account{
	private $id, $username, $settings, $verified;

	function __construct($id, $username, $settings, $verified){
		$this->id = $id;
		$this->username = $username;
		$this->settings = $settings;
		$this->verified = $verified;
	}

	public function getAccountID(){ return $this->id; }

	public function getUsername(){ return $this->username; }

	public function getSettings(){ return $this->settings; }

	public function isVerified(){ return $this->verified; }

	public abstract function checkPassword($enteredPassword);
}
?>